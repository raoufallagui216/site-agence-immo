import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './layouts/nav-bar/nav-bar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { SearchComponent } from './components/search/search.component';
import { RouterModule } from '@angular/router';
import { ListAnnonceComponent } from './components/list-annonce/list-annonce.component';



@NgModule({
  declarations: [
    NavBarComponent,
    FooterComponent,
    SearchComponent,
    ListAnnonceComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports : [
    NavBarComponent,
    FooterComponent,
    SearchComponent,
    ListAnnonceComponent
  ]
})
export class SharedModule { }
