import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnnonceRoutingModule } from './annonce-routing.module';
import { AnnonceComponent } from './components/annonce/annonce.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    AnnonceComponent
  ],
  imports: [
    CommonModule,
    AnnonceRoutingModule,
    SharedModule
  ]
})
export class AnnonceModule { }
